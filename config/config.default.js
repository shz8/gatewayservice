'use strict';

module.exports = (appInfo) => {
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1546587653841_2867';

  // add your config here
  config.middleware = [];
  exports.mongo = {
    client: {
      host: 'd.vy01.com',
      port: '27017',
      name: 'gateway',
      user: 'zs',
      password: 'zs123456',
      options: {
        authSource: 'admin',
      },
    },
  };
  config.redis = {
    client: {
      port: 6379, // Redis port
      host: 'd.vy01.com', // Redis host
      password: 'zht111',
      db: 1,
    },
  };
  exports.security = {
    csrf: {
      enable: false,
    },
  };
  return config;
};

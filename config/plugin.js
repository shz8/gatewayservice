'use strict';

// had enabled by egg
// exports.static = true;
exports.mongo = {
  enable: true,
  package: 'egg-mongo-native',
};
exports.redis = {
  enable: true,
  package: 'egg-redis',
};
'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    let apis = await this.app.mongo.find('apis', {});
    console.dir(apis)
    let data = await this.ctx.app.redis.get(`abc`)
    console.dir(data)
    this.ctx.body = {apis,data}
  }
}

module.exports = HomeController;
